package com.formacionbdi.springboot.app.item.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.formacionbdi.springboot.app.item.models.Item;
import com.formacionbdi.springboot.app.item.models.Producto;
import com.formacionbdi.springboot.app.item.models.service.ItemService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

/**
 * The Class ItemController.
 * here is used @RefreshScope annotation in order to refresh properties without restarting the app
 * to perform refreshing is used the following endpoint:
 * localhost:8005/actuator/refresh
 */
@RefreshScope
@RestController
public class ItemController {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(ItemController.class);
	
	/** The env. */
	@Autowired
	private Environment env;

	/** The item service. */
	@Autowired
	private ItemService itemService;

	/** The texto. */
	@Value("${configuracion.texto}")
	private String texto;

	/**
	 * Listar.
	 *
	 * @return the list
	 */
	@GetMapping("/listar")
	public List<Item> listar() {
		return itemService.findAll();
	}

	/**
	 * Detalle.
	 *
	 * @param id the id
	 * @param cantidad the cantidad
	 * @return the item
	 */
	@HystrixCommand(fallbackMethod = "metodoAlternativo")
	@GetMapping("/ver/{id}/cantidad/{cantidad}")
	public Item detalle(@PathVariable Long id, @PathVariable Integer cantidad) {
		return itemService.findById(id, cantidad);
	}

	/**
	 * Metodo alternativo.
	 *
	 * @param id the id
	 * @param cantidad the cantidad
	 * @return the item
	 */
	public Item metodoAlternativo(Long id, Integer cantidad) {
		Item item = new Item();
		Producto producto = new Producto();
		item.setCantidad(cantidad);
		producto.setId(id);
		producto.setNombre("Camara Sony");
		producto.setPrecio(500.00);
		item.setProducto(producto);
		return item;
	}

	/**
	 * Obener config.
	 *
	 * @param puerto the puerto
	 * @return the response entity
	 */
	@GetMapping("/obtener-config")
	public ResponseEntity<?> obenerConfig(@Value("${server.port}") String puerto) {
		log.info(texto);
		Map<String, String> json = new HashMap<>();
		json.put("texto", texto);
		json.put("puerto", puerto);
		
		if (env.getActiveProfiles().length>0 && env.getActiveProfiles()[0].equals("dev")) {
			json.put("autor.nombre", env.getProperty("configuracion.autor.nombre"));
			json.put("autor.email", env.getProperty("configuracion.autor.email"));
		}

		return new ResponseEntity<Map<String, String>>(json, HttpStatus.OK);
	}
}
